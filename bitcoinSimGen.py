# NAME: Charles Rawlins
# Func: bitcoinSimGen
# Desc: Calculates the simulated Bitcoin proof of work for the Omnet++ Simulation.

# Resources for code and learning:
# https://gist.github.com/ChristopherJohnston/42b4a8af11c2d02ef2cc2c972fe6ee5d
# http://www.righto.com/2014/02/bitcoin-mining-hard-way-algorithms.html
# https://gist.github.com/shirriff/cd5c66da6ba21a96bb26#file-mine-py
# https://gist.github.com/shirriff/cd5c66da6ba21a96bb26#file-mine-py
# https://en.bitcoin.it/wiki/Difficulty

import hashlib
import merkletools
import csv
import binascii
import time

# Handles block information and nonce/hash generation for the given dummy transaction data.
class Blockinfo:

    def __init__(self, data, indexnum, prevhash ):
        self.version = 1
        self.index = indexnum
        self.timestamp = indexnum
        self.trxnData = data
        self.prevHash = prevhash
        self.blockHash = []
        self.blockNonce = []
        self.blockDifficulty = []

        # Calculate merkle hash and nonce
        self.merkle = self.calcMerkle()
        self.calculatedhash = []
        self.nonce = []
        self.calcHash()

    # Calculates the merkle root for the proof of work with the input dummy trxn data.
    def calcMerkle(self):
        mt = merkletools.MerkleTools(hash_type="sha256")
        for i in self.trxnData:
            mt.add_leaf(i, True)

        mt.make_tree()
        return mt.get_merkle_root()

    # Calculates the target difficulty value given the input difficulty bits.
    def calculate_target(self, bits):

        exp = bits >> 24
        mant = bits & 0xffffff
        target_hexstr = '%064x' % (mant * (1 << (8 * (exp - 3))))
        return target_hexstr


    # Main function to find the proof of work difficulty hash. 
    def calcHash(self):

        nonce = 0
        starttime = time.time()
        print("Started block #" + str(self.index) + "!")

        difficultybits = 0x1f00ffff # Easier sim difficulty for calculations on laptop/lighter node
        # difficultybits = 0x1d00ffff # Easiest Bitcoin difficulty for full-powered node or Foundry

        self.blockDifficulty = str(hex(difficultybits))

        # Concatenate block data for hash input.
        verstring = "1" # Always version 1 for simulation
        hashstr = verstring + str(self.prevHash) + str(self.merkle) + str(self.timestamp)  + self.blockDifficulty +  str(difficultybits) + str(nonce)
        hashstr = str.encode(hashstr)

        target = self.calculate_target(difficultybits)
        targetval = int(target,16)

        while True:

            # Check if nonce has exceeded 32 bits, if so, increment the timestamp to change the input and try to find the correct hash again...
            if nonce >= (2 **32):
                timeelapsed = time.time() - starttime
                timeelapsed = "{:.2f}".format(timeelapsed)
                print("Failed! Time elapsed: " + timeelapsed + " seconds")
                print("Incrementing timestamp!")
                self.timestamp += 1
                # Recalculate hashstring
                hashstr = verstring + str(self.prevHash) + str(self.merkle) + str(self.timestamp)  + self.blockDifficulty +  str(difficultybits) + str(nonce)
                hashstr = str.encode(hashstr)
                nonce = 0
                # If 100 epochs occur of incrementing the timestamp, break the loop.
                if self.timestamp > 100:
                    print("Couldn't find hash after a long time!")
                    break

            nonce_bytes = nonce.to_bytes(4, 'big')
            hashval = hashlib.sha256(hashlib.sha256(nonce_bytes + hashstr).digest()).digest()

            compareval = int(binascii.hexlify(hashval).decode('utf-8'),16)
            nonce += 1

            if compareval < targetval:
                # Found a hash value with zeros lower than the target! Success!
                timeelapsed = time.time() - starttime
                timeelapsed = "{:.2f}".format(timeelapsed)
                print("Success! Time elapsed: " + timeelapsed + " seconds")
                foundit = True
                self.calculatedhash = binascii.hexlify(hashval).decode('utf-8')
                self.nonce = binascii.hexlify(nonce_bytes).decode('utf-8')
                break

        # Handles if hash calculations have proceeded for a very long time.
        if foundit == False:
            self.nonce = 0x00
            self.calculatedhash = 0x00
            timeelapsed = time.time() - starttime
            timeelapsed = "{:.2f}".format(timeelapsed)
            print("Took too long! Time elapsed: " + timeelapsed + " seconds")



if __name__ == "__main__":

    # Dummy transaction data for block generation.
    trxninput = [["CharlestoJag","CharlestoEric"],["CharlestoCharles","CharlestoKyle"],
                         ["JagtoKyle","JagtoJag"],["CharlestoJag","ErictoEric"]] # Four Blocks, two transactions each.

    blockList = []

    print("Starting BitcoinSimGen!")

    # Generate genesis block similar to real-world Bitcoin genesis block. 
    genesisBlock = Blockinfo(trxninput[0], 1, '0000000000000000000000000000000000000000000000000000000000000000') # 64 0s
    print("Genesis Nonce = " + genesisBlock.nonce)

    print("Created Genesis")
    blockList.append(genesisBlock)

    # Generate block data sequentially with genesis block data.
    j = 2
    for i in range(len(trxninput)):
        blockList.append(Blockinfo(trxninput[i], j, blockList[i].calculatedhash))
        j = j+1

    print("Created list of blocks!")

    # Write block output to .csv file for reading in Omnet++
    filename = 'bitcoinSimData.csv'

    with open(filename,'w',newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for i in blockList:
            outwrite = [str(i.index), str(i.merkle), str(i.calculatedhash), str(i.nonce),
                        str(i.prevHash), str(i.timestamp), str(i.blockDifficulty), str(i.trxnData)]
            writer.writerow(outwrite)
